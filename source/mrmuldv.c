/* This file selects the correct implementation from the compiler macros.
 * you can override this by defining MR_MULDV macro at compile time
 */

/*
 * GNU Compiler
 */
#if defined(__GNUC__)

/* AMD64 inlined ASM */
#if !defined(MR_MULDV) && defined(__amd64__)
#define MR_MULDV "mrmuldv-gcc-amd64.c"
#endif

/* x86 inlined ASM */
#if !defined(MR_MULDV) && defined(__i386__)
#define MR_MULDV "mrmuldv-gcc-x86.c"
#endif

/* PPC inlined ASM */
#if !defined(MR_MULDV) &&  defined(__ppc__)
#define MR_MULDV "mrmuldv-gcc-ppc.c"
#endif

#endif // __GNUC__

/*
 * Microsoft Compiler
 */
#if !defined(MR_MULDV) && defined(_MSC_VER)
#error "FIXME: support MSVC compiler"
#endif

/*
 * Use STD C implementation as a last resort
 */
#if !defined(MR_MULDV)
#define MR_MULDV "mrmuldv-c.c"
#endif

#include MR_MULDV
