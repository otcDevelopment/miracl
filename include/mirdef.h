/*
 *   MIRACL compiler/hardware definitions - mirdef.h
 */

#include <limits.h>
#include <stdint.h>

#if __GNUC__
/* Use GCC predefined macros*/
#ifndef __BYTE_ORDER__
# error "GNUC: Can't detect byte order"
#endif

/* Detect BYTE Order */
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
#define MR_LITTLE_ENDIAN
#else
#define MR_BIG_ENDIAN
#endif

#if defined(__amd64__)

#define MIRACL      64
#define mr_utype    long
#define MR_IBITS    32
#define MR_LBITS    64

#define mr_unsign32 uint32_t
#define mr_unsign64 uint64_t

#endif

#endif

/* A double length type must be sizeof(mr_utype) * 2
 *
 * #define mr_dltype
 */

//#define MR_STATIC 8

/*
 * Always using a power-of-2 (or 0) as a number base reduces code space
 * and will also be a little faster. This is recommended.
 * Will all of your programs use a power-of-2 as a number base (Y/N)?
 * #define MR_ALWAYS_BINARY
*/

#define MAXBASE ((mr_small)1<<(MIRACL-1))

/*
 * creates a file mrkcm.c from mrkcm.tpl and ms86.mcs. The KCM method will then
 * be optimised for moduli of sizes 512, 1024, 2048 bits etc. Typically this
 * might be used for a fast implementation of RSA, DSS or Diffie-Hellman.
 */
//#define MR_KCM 8

#if !defined(MR_BITSINCHAR) && defined(CHAR_BIT)
#define MR_BITSINCHAR CHAR_BIT
#else
#error "CHAR_BIT undefined"
#endif

/*
 * Define the size of the mantissa
 * #define MR_FLASH <manti>
 */

#define MR_FLASH 52
